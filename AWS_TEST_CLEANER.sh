#!/bin/bash

grep -v "Pacing" $1/AWS-TEST.jtl > $1/AWS-TEST-CLEAN.jtl
sed '/Ping/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/-XX-/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/-BT_/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/_XX/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Final_screenshot/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get empty partons array for C111638 & C112406/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get unchecked items array for C112406 & C111638/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get bib records array for C111599/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get parton-item pairs array for C112407 & C112824/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get fined patrons array for C112831/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Create cases object/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Create an array of offsets/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/setUp Thread Group_009_Get_titles_for_C000001 and C000004/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/setUp Thread Group_010_Get_subjects_for_C000002 and C000004/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/setUp Thread Group_011_Get_call_nums_for_C000003 and C000004/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl

#Creates special file for "Searches" calculation
grep timeStamp $1/AWS-TEST.jtl > $1/AWS-SEARCH.jtl
grep BT_SEARCH_ITEM_BY_AUTHOR $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' | sed 's/(Incrorrect_search)//' >> $1/AWS-SEARCH.jtl
grep BT_SEARCH_ITEM_BY_TITLE $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' >> $1/AWS-SEARCH.jtl
grep BT_SEARCH_ITEM_BY_BARCODE $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' >> $1/AWS-SEARCH.jtl
grep BT_SEARCH_ITEM_BY_SUBJECT $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' >> $1/AWS-SEARCH.jtl
grep BT_SEARCH_ITEM_BY_CALL_NUMBER $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' >> $1/AWS-SEARCH.jtl
grep BT_SEARCH_ITEM_BY_KEYWORD $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' >> $1/AWS-SEARCH.jtl
grep BT_SEARCH_PATRON_BY_BARCODE $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' >> $1/AWS-SEARCH.jtl

grep timeStamp $1/AWS-TEST.jtl > $1/AWS-CHECK.jtl
grep BT_CHECK_IN $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' | sed 's/_NO_PATRON//' >> $1/AWS-CHECK.jtl
grep BT_CHECK_OUT $1/AWS-TEST.jtl | sed 's/C[0-9]*-//' | sed 's/_SINGLE_ITEM//' | sed 's/_MULTIPLE_ITEMS//' >> $1/AWS-CHECK.jtl
