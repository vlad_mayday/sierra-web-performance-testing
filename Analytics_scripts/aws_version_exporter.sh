AMI=$(aws --output text --region ${1} cloudformation describe-stacks --stack-name ${2} --query "Stacks[0].Parameters[?contains(ParameterKey, 'AmiId')].ParameterValue")
aws --output table ec2 --region ${1} describe-images --image-ids ${AMI} --query "sort_by(Images[].{Role: (Tags[?Key == 'Role'].Value)[0], Version: (Tags[?Key == 'Docker Tag'].Value)[0]}, &Role)" > versions
#--region ${2}