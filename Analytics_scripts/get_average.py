import sys

f = open(sys.argv[1], 'r')
# result = open(sys.argv[1]+'.average_parsed.csv', 'w')
offset = 0
offsetEnd = 999999
if len(sys.argv) > 2:
    offset = int(sys.argv[2])
if len(sys.argv) == 4:
    offsetEnd = int(sys.argv[3])
	
summ = 0
counter = 0
realCounter = 0
for line in f:
    if counter == 1:
        startTime = int(line.split(',')[0][:-3])
    if counter != 0 and (startTime + offset) < int(line.split(',')[0][:-3]) and (startTime + offsetEnd) > int(line.split(',')[0][:-3]):
        realCounter += 1
        temp_string = line.split(',')[1][:-3]
        if len(temp_string) > 0:
            summ += int(temp_string)
    counter += 1
        
realCounter = max(1, realCounter)
# result.write(str(summ/realCounter))
print (str(summ/realCounter))