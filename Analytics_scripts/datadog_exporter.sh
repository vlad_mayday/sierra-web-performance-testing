api_key=${6}
application_key=${7}

currentTime=$(date +%s%N | cut -b1-13)
offsetFrom=$(((${currentTime}/1000)-${4}))
offsetTo=$(((${currentTime}/1000)-${5}))

#echo "test"
#echo ${currentTime}
#echo ${offsetFrom}
#echo ${offsetTo}

for SERVICE_NAME in $2
do
   curl -G "https://api.datadoghq.com/api/v1/hosts"   -s   -d "api_key=${api_key}"      -d "application_key=${application_key}"  -d "filter=environment:${1},role:${SERVICE_NAME}" > ${SERVICE_NAME}_raw
   grep  -Po '(?<=\{")name":"([0-9a-zA-Zi-]+)' ${SERVICE_NAME}_raw | sed "s/name\":\"//g" > ${SERVICE_NAME}_list.txt
   grep  -Po '(?<=\")name\":\"(.*?)\"is_muted'\|'(?<=\"gohai\":\")(.*?)\"host_id' ${SERVICE_NAME}_raw | sed "s/\",\"host_id//g" | sed "s/\",\"is_muted//g" | sed "s/{/\n/g"  | sed "s/}/\n/g" | sed "s/\"//g" | sed "s/\\\//g" | sed "s/\,/\n/g" | sed "s/\[//g" | sed "s/\]//g" > ${SERVICE_NAME}_HARDWARE.txt
   
   if [ "${SERVICE_NAME}" = "dbinstance" ]; then
	   curl -G "https://api.datadoghq.com/api/v1/hosts"  -s    -d "api_key=${api_key}"      -d "application_key=${application_key}"  -d "filter=environment:${1},resource:${SERVICE_NAME}" > ${SERVICE_NAME}_db_raw
	   grep  -Po '(?<=\{")name":"([0-9a-zA-Zi-]+)' ${SERVICE_NAME}_db_raw | sed "s/name\":\"//g" > ${SERVICE_NAME}_db_list.txt
   fi
	while read HOST; do
		
		for METRIC in $3
		do
			
			if [ "${METRIC}" = "LOG" ]; then
				curl  --cookie "dogweb=564ed6ef5caceca92d71c32911d6b88ba886afd7f2ca9ccb4ac5496fb1a98fddf4a150c4"   -s  -d "query={\"list\":{\"time\":{\"offset\":-7200000,\"from\":\"now-${offsetFrom}s\",\"to\":\"now-${offsetTo}s\"},\"search\":{\"query\":\"host:${HOST}\"},\"columns\":[{\"field\":{\"path\":\"host\"}},{\"field\":{\"path\":\"service\"}}],\"limit\":5000,\"sort\":{\"time\":{\"order\":\"desc\"}}}}" -G "https://api.datadoghq.com/api/v1/logs-queries/scopes/4749/list-csv" > ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.log
				
				tar -zcvf ${SERVICE_NAME}_${METRIC}_${HOST}.tar.gz ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.log
			else
				curl -G  "https://api.datadoghq.com/api/v1/query" -s  -d "api_key=${api_key}"   -d "application_key=${application_key}"   -d "from=${4}"   -d "to=${5}"   -d "query=${METRIC}{host:${HOST}}"  -o ${SERVICE_NAME}_${METRIC}_${HOST}.json
				echo "timeStamp,elapsed,label,responseCode,responseMessage,threadName,success,bytes,grpThreads,allThreads,Latency,Hostname" > ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.jtl
			
				 grep -Po '(?<=\[)[0-9.,]+' ${SERVICE_NAME}_${METRIC}_${HOST}.json  |  sed "s/\$/,DATADOG,,DATADOG_METRIC,,true,0,0,0,0,${SERVICE_NAME}/g" | sed "s/\.0,/,/g"  |  sed -E "s/,0/,/g " | sed -E "s/,,,,,/,0,0,0,0,/g" |  awk '{split($0,a,","); print a[1]  "," int((a[2]*10000)/10) ","  a[3] "," a[4] "," a[5] "," a[6] "," a[7] "," a[8] "," a[9] "," a[10] "," a[11] "," a[12]}'  >> ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.jtl
				
				Jmeter_instance/bin/JMeterPluginsCMD.sh --tool Reporter --generate-png ${SERVICE_NAME}_${METRIC}_${HOST}.png --input-jtl ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.jtl --plugin-type PerfMon --relative-times  no --width 1920 --height 1080 --auto-scale no > /dev/null
				
			fi
			
		done
		
	done < ${SERVICE_NAME}_list.txt
	
	if [ -f "${SERVICE_NAME}_db_list.txt" ]
	then
		while read HOST; do
			
			for METRIC in $3
			do
				
				if [ "${METRIC}" = "LOG" ]; then
					curl  --cookie "dogweb=564ed6ef5caceca92d71c32911d6b88ba886afd7f2ca9ccb4ac5496fb1a98fddf4a150c4"   -s  -d "query={\"list\":{\"time\":{\"offset\":-7200000,\"from\":\"now-${offsetFrom}s\",\"to\":\"now-${offsetTo}s\"},\"search\":{\"query\":\"host:${HOST}\"},\"columns\":[{\"field\":{\"path\":\"host\"}},{\"field\":{\"path\":\"service\"}}],\"limit\":5000,\"sort\":{\"time\":{\"order\":\"desc\"}}}}" -G "https://api.datadoghq.com/api/v1/logs-queries/scopes/4749/list-csv" > ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.log
					
					tar -zcvf ${SERVICE_NAME}_${METRIC}_${HOST}.tar.gz ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.log
				else
					curl -G  "https://api.datadoghq.com/api/v1/query" -s  -d "api_key=${api_key}"   -d "application_key=${application_key}"   -d "from=${4}"   -d "to=${5}"   -d "query=${METRIC}{*}by{${HOST}}"  -o ${SERVICE_NAME}_${METRIC}_${HOST}.json
					echo "timeStamp,elapsed,label,responseCode,responseMessage,threadName,success,bytes,grpThreads,allThreads,Latency,Hostname" > ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.jtl
				
					 grep -Po '(?<=\[)[0-9.,]+' ${SERVICE_NAME}_${METRIC}_${HOST}.json  |  sed "s/\$/,DATADOG,,DATADOG_METRIC,,true,0,0,0,0,${SERVICE_NAME}/g" | sed "s/\.0,/,/g"  |  sed -E "s/,0/,/g " | sed -E "s/,,,,,/,0,0,0,0,/g" |  awk '{split($0,a,","); print a[1]  "," int((a[2]*10000)) ","  a[3] "," a[4] "," a[5] "," a[6] "," a[7] "," a[8] "," a[9] "," a[10] "," a[11] "," a[12]}'  >> ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.jtl
					
					Jmeter_instance/bin/JMeterPluginsCMD.sh --tool Reporter --generate-png ${SERVICE_NAME}_${METRIC}_${HOST}.png --input-jtl ${SERVICE_NAME}_${METRIC}_${HOST}_datadog.jtl --plugin-type PerfMon --relative-times  no --width 1920 --height 1080 --auto-scale no > /dev/null
					
				fi
				
			done
			
		done < ${SERVICE_NAME}_db_list.txt
	fi
done
 