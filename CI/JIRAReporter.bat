@echo off

set host=%1
set login=%2
set password=%3
set summary=%~4
set description=%~5
set path_to_screens=%~6

set send=C:\Curl\bin\curl.exe -u %login%:%password% -H "Content-Type: application/json" -X POST -d "{\"fields\": {\"project\":{\"id\": \"11510\",\"key\": \"SIERRA\"},\"labels\": [\"EPAM\",\"EPAM_report\"],\"summary\": \"%summary%\",\"description\": \"%description%\",\"issuetype\": {\"name\": \"Story\"}}}" https://%host%/rest/api/2/issue/

FOR /F "tokens=*" %%F IN ('%send%') DO (
SET var=%%F
)

rem Remove quotes
set var=%var:"=%
rem Remove braces
set "var=%var:~1,-1%"
rem Change colon by equal-sign
set "var=%var::==%"
rem Separate parts at comma into individual assignments
set "%var:,=" & set "%"
 echo %id%
echo %path_to_screens%

FOR /F "tokens=* USEBACKQ" %%F IN (`dir %path_to_screens%\*.png /b`) DO (
C:\Curl\bin\curl.exe -D- -u %login%:%password% -X POST -H "X-Atlassian-Token: nocheck" -F "file=@%path_to_screens%\%%F" https://%host%/rest/api/2/issue/%id%/attachments
)