Jmeter_instance_SierraWeb/bin/JMeterPluginsCMD.sh --tool Reporter --generate-png $BUILD_NUMBER/HitsPerSecond.png --input-jtl $BUILD_NUMBER/AWS-TEST-CLEAN.jtl --plugin-type HitsPerSecond --width 1920 --height 1080

Jmeter_instance_SierraWeb/bin/JMeterPluginsCMD.sh --tool Reporter --generate-png $BUILD_NUMBER/ThreadsStateOverTime.png --input-jtl $BUILD_NUMBER/AWS-TEST-CLEAN.jtl --plugin-type ThreadsStateOverTime --width 1920 --height 1080 --aggregate-rows yes

Jmeter_instance_SierraWeb/bin/JMeterPluginsCMD.sh --tool Reporter --generate-png $BUILD_NUMBER/ResponseTimesOverTime.png --input-jtl $BUILD_NUMBER/AWS-TEST-CLEAN.jtl --plugin-type ResponseTimesOverTime --exclude-label-regex true --width 1920 --height 1080 --aggregate-rows yes

Jmeter_instance_SierraWeb/bin/JMeterPluginsCMD.sh --tool Reporter --generate-png $BUILD_NUMBER/ResponseCodesPerSecond.png --input-jtl $BUILD_NUMBER/AWS-TEST-CLEAN.jtl --plugin-type ResponseCodesPerSecond --width 1920 --height 1080

Jmeter_instance_SierraWeb/bin/JMeterPluginsCMD.sh --tool Reporter --generate-png $BUILD_NUMBER/TransactionsPerSecond.png --input-jtl $BUILD_NUMBER/AWS-TEST-CLEAN.jtl --plugin-type TransactionsPerSecond --width 1920 --height 1080 --aggregate-rows yes

Jmeter_instance_SierraWeb/bin/JMeterPluginsCMD.sh --tool Reporter --generate-csv $BUILD_NUMBER/Report.csv --input-jtl $BUILD_NUMBER/AWS-TEST.jtl --plugin-type AggregateReport

Jmeter_instance_SierraWeb/bin/JMeterPluginsCMD.sh --tool Reporter --generate-csv $BUILD_NUMBER/CLEAN_Report.csv --input-jtl $BUILD_NUMBER/AWS-TEST-CLEAN.jtl --plugin-type AggregateReport

