#!/bin/bash

grep -v "Pacing" $1/AWS-TEST.jtl > $1/AWS-TEST-CLEAN.jtl
sed '/Final_screenshot/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Authorization_Timer/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get patrons for ""Add hold...""/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get items for ""Add hold...""/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get patrons for ""Get_patron_holds"" cases/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get patrons for ""Get_patron_fines"" cases/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get patrons for ""Get_patron checkouts""/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/Get holds for ""Modify_patron_hold""/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl
sed '/choose_token/d' $1/AWS-TEST-CLEAN.jtl > $1/AWS-TEST_temp && mv $1/AWS-TEST_temp $1/AWS-TEST-CLEAN.jtl