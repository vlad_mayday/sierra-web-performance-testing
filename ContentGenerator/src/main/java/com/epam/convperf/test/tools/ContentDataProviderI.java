package com.epam.convperf.test.tools;

import java.util.ArrayList;

import com.epam.convperf.test.tools.datamodel.Agent;
import com.epam.convperf.test.tools.datamodel.Concept;
import com.epam.convperf.test.tools.datamodel.Instance;
import com.epam.convperf.test.tools.datamodel.Item;
import com.epam.convperf.test.tools.datamodel.Work;

/**
 * Unified interface for all content providers
 * @author Sergey_Kandyrin
 *
 */
public interface ContentDataProviderI {

	/**
	 * Return full Agent list (actually not used)
	 */
	public ArrayList<Agent> getAgentList();

	/**
	 * Return full Agent list (actually not used)
	 */
	public ArrayList<Concept> getConceptList();

	/**
	 * Return the next agent.
	 * But some providers can store a limited number of entities because source file is limited
	 * (is about MARC, BIBFRAME content provider)
	 * However, the RandomDataProvider can export entities without any limitation
	 */
	public Agent getNextAgent();

	/**
	 * Return the next concept.
	 * But some providers can store a limited number of entities because source file is limited
	 * (is about MARC, BIBFRAME content provider)
	 * However, the RandomDataProvider can export entities without any limitation
	 */
	public Concept getNextConcept();

	/**
	 * This method replaces the UID value for the corresponding entity in the store
	 * @param oldUID - original UID
	 * @param registredUID - server generated UID
	 */
	public void updateAgentID(String oldUID, String registredUID);

	/**
	 * This method replaces the UID value for the corresponding entity in the store
	 * @param oldUID - original UID
	 * @param registredUID - server generated UID
	 */
	public void updateConceptID(String oldUID, String registredUID);

	/**
	 * Return the next work.
	 * But some providers can store a limited number of entities because source file is limited
	 * (is about MARC, BIBFRAME content provider)
	 * However, the RandomDataProvider can export entities without any limitation
	 */
	public Work getNextWork();

	/**
	 * IN PROGRESS
	 */
	public Instance getNextInstanceByCurrentWork();
	
	/**
	 * IN PROGRESS
	 */
	public Item getNextItemByCurrentWork();

}
