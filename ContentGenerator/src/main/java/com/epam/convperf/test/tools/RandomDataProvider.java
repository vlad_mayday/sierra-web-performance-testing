package com.epam.convperf.test.tools;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.epam.convperf.test.tools.datamodel.Agent;
import com.epam.convperf.test.tools.datamodel.Concept;
import com.epam.convperf.test.tools.datamodel.Instance;
import com.epam.convperf.test.tools.datamodel.Item;
import com.epam.convperf.test.tools.datamodel.Work;

/**
 * The implementation of the data reader and content store system for the RANDOM format
 * @author Sergey_Kandyrin
 * 
 * @see ContentDataProviderI 
 *
 */
public class RandomDataProvider implements ContentDataProviderI{

private static Logger logger = Logger.getLogger(RandomDataProvider.class);
	

    ScriptEngineManager mgr = new ScriptEngineManager();
    ScriptEngine engine = mgr.getEngineByName("JavaScript");
    
	static private ArrayList<Agent> agentList = new ArrayList<>();
	static private ArrayList<Concept> conceptList = new ArrayList<>();
	
	
	private static JSONObject agentDataModel;
	private static JSONObject agentMutationRule;
	private static JSONObject agentGenerationRule;
	private static JSONObject conceptDataModel;
	private static JSONObject conceptMutationRule;
	private static JSONObject conceptGenerationRule;
	private static JSONObject workDataModel;
	private static JSONObject workMutationRule;
	private static JSONObject workGenerationRule;
	
	static {
		JSONParser parser = new JSONParser();
		//URL resource;
		Object obj = null;

		try {

			obj = parser.parse(new FileReader("agent.json"));
			agentDataModel =  (JSONObject) obj;

			obj = parser.parse(new FileReader("agent_mutation_rule.json"));
			agentMutationRule =  (JSONObject) obj;
			
			obj = parser.parse(new FileReader("agent_generation_rule.json"));
			agentGenerationRule =  (JSONObject) obj;

			obj = parser.parse(new FileReader("concept.json"));
			conceptDataModel =  (JSONObject) obj;

			obj = parser.parse(new FileReader("concept_mutation_rule.json"));
			conceptMutationRule =  (JSONObject) obj;
			
			obj = parser.parse(new FileReader("concept_generation_rule.json"));
			conceptGenerationRule =  (JSONObject) obj;


			obj = parser.parse(new FileReader("work.json"));
			workDataModel =  (JSONObject) obj;

			obj = parser.parse(new FileReader("work_mutation_rule.json"));
			workMutationRule =  (JSONObject) obj;
			
			obj = parser.parse(new FileReader("work_generation_rule.json"));
			workGenerationRule =  (JSONObject) obj;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public ArrayList<Agent> getAgentList() {
		return agentList;
	}

	@Override
	public ArrayList<Concept> getConceptList() {
		return conceptList;
	}

	@Override
	public Agent getNextAgent() {
		Agent agent = new Agent();
		Set<String> keySet = agentGenerationRule.keySet();
		for (String key : keySet) {
			JSONArray fieldList = (JSONArray)agentGenerationRule.get(key);
			for (Object field :fieldList) {
				try {
					
					agent.addDataField(field.toString(), engine.eval(key).toString());
				} catch (ScriptException e) {
					e.printStackTrace();
				}
			}
			
		}
		agentList.add(agent);
		return agent;
	}

	@Override
	public Concept getNextConcept() {
		Concept concept = new Concept();
		Set<String> keySet = conceptGenerationRule.keySet();
		for (String key : keySet) {
			JSONArray fieldList = (JSONArray)conceptGenerationRule.get(key);
			for (Object field :fieldList) {
				try {
					
					concept.addDataField(field.toString(), engine.eval(key).toString());
				} catch (ScriptException e) {
					e.printStackTrace();
				}
			}
			
		}
		conceptList.add(concept);
		return concept;
	}

	@Override
	public void updateAgentID(String oldUID, String registredUID) {
		for (Agent agent : agentList) {
			if(agent.getUID().equals(oldUID)) {
				agent.setUID(registredUID);
				logger.debug(String.format("Update agent uid %s -> %s", oldUID, registredUID));
				break;
			}
		}
		
	}

	@Override
	public void updateConceptID(String oldUID, String registredUID) {
		for (Concept concept : conceptList) {
			if(concept.getUID().equals(oldUID)) {
				concept.setUID(registredUID);
				logger.debug(String.format("Update concept uid %s -> %s", oldUID, registredUID));
				break;
			}
		}
		
	};

	@Override
	public Work getNextWork() {
		Work work = new Work();
		Set<String> keySet = workGenerationRule.keySet();
		for (String key : keySet) {
			JSONArray fieldList = (JSONArray)workGenerationRule.get(key);
			for (Object field :fieldList) {
				try {
					
					work.addDataField(field.toString(), engine.eval(key).toString());
				} catch (ScriptException e) {
					e.printStackTrace();
				}
			}
			
		}
		return work;
	}

	@Override
	public Instance getNextInstanceByCurrentWork() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Item getNextItemByCurrentWork() {
		// TODO Auto-generated method stub
		return null;
	}

}
