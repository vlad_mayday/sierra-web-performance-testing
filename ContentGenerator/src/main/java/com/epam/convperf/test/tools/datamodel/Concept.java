package com.epam.convperf.test.tools.datamodel;

import java.util.ArrayList;

import com.epam.convperf.test.tools.MarcDataProvider;

public class Concept extends CEEntity {

	public static String CONCEPT_MARC_FIELD = "650";

	public Concept() {
		super();
	}

	public static ArrayList<Concept> parseConcepts(String rawConceptText) {
		rawConceptText = rawConceptText.replace("$a", ",").replace("$x", ",").replace("$y", ",").replace("$z", ",")
				.replace("$v", ",");

		ArrayList<Concept> conceptList = new ArrayList<>();
		String[] concepts = rawConceptText.split(",");
		for (int i = 1; i < concepts.length; i++) {
			Concept uniqConcept = new Concept();
			uniqConcept.addDataField(CONCEPT_MARC_FIELD, concepts[i]);
			conceptList.add(uniqConcept);
		}
		return conceptList;

	}

	public void addDataField(String type, String value) {
		data.put(type, value);
	}

	public String toJson() {
		return MarcDataProvider.mergeConceptEntity(data).toJSONString();
	}

}
