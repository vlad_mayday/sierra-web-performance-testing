package com.epam.convperf.test.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import com.epam.convperf.test.tools.datamodel.Agent;
import com.epam.convperf.test.tools.datamodel.Concept;
import com.epam.convperf.test.tools.datamodel.Instance;
import com.epam.convperf.test.tools.datamodel.Item;
import com.epam.convperf.test.tools.datamodel.Work;

/**
 * The implementation of the data reader and content store system for the BIBFRAME format
 * @author Sergey_Kandyrin
 *
 */
public class BibFrameDataProvider implements ContentDataProviderI {

	@Override
	public ArrayList<Agent> getAgentList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Concept> getConceptList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public synchronized Agent getNextAgent() {
		return null;
	}

	@Override
	public synchronized Concept getNextConcept() {
		return null;
	}

	@Override
	public Work getNextWork() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Instance getNextInstanceByCurrentWork() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Item getNextItemByCurrentWork() {
		// TODO Auto-generated method stub
		return null;
	}

	public BibFrameDataProvider(File marcResource) throws FileNotFoundException {

	}

	@Override
	public void updateAgentID(String oldUID, String registredUID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateConceptID(String oldUID, String registredUID) {
		// TODO Auto-generated method stub

	}

}
