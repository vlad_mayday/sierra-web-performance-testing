package com.epam.convperf.test.jmeter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import org.json.simple.parser.ParseException;

import com.epam.convperf.test.tools.ContentDataProviderI;
import com.epam.convperf.test.tools.MarcDataProvider;
import com.epam.convperf.test.tools.RandomDataProvider;
import com.epam.convperf.test.tools.datamodel.Agent;
import com.epam.convperf.test.tools.datamodel.Concept;
import com.epam.convperf.test.tools.datamodel.Work;
import com.google.common.io.Resources;

public class Test {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {

		URL data = Resources.getResource("50000-records_mins.mrc");
		ContentDataProviderI marcProvider = new MarcDataProvider(new File(data.getFile()));
		Agent agent1 = marcProvider.getNextAgent();
		System.out.println(agent1.toJson());
		
		ContentDataProviderI ramdomProvider = new RandomDataProvider();
		Agent agent2 = ramdomProvider.getNextAgent();
		System.out.println(agent2.toJson());
		
		/*
		Concept concept;
		Agent agent;

		while ((concept = provider.getNextConcept()) != null) {
			System.out.println(concept.getUID());
			System.out.println(concept.toJson());
		}

		while ((agent = provider.getNextAgent()) != null) {
			System.out.println(agent.getUID());
			System.out.println(agent.toJson());
		}
		;
		// System.out.println(concept.toJson());

		// Agent agent = provider.getNextAgen();
		// System.out.println(agent.toJson());
*/
		
		
		// Work work = provider.getNextWork();
		// System.out.println(work.toJson());
	}

}
