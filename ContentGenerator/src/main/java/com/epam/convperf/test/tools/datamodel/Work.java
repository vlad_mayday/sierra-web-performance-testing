package com.epam.convperf.test.tools.datamodel;

import com.epam.convperf.test.tools.MarcDataProvider;

public class Work extends CEEntity{
	public static String WORK_MARC_FIELD = "100";

	public void addDataField(String type, String value) {
		data.put(type, value);
	}

	public String toJson() {
		return MarcDataProvider.mergeWorkEntity(data).toJSONString();
	}

}
