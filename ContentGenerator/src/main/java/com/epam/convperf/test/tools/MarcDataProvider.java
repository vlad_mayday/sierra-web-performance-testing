package com.epam.convperf.test.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.marc4j.MarcStreamReader;
import org.marc4j.marc.Record;
import org.marc4j.marc.VariableField;

import com.epam.convperf.test.tools.datamodel.Agent;
import com.epam.convperf.test.tools.datamodel.Concept;
import com.epam.convperf.test.tools.datamodel.Instance;
import com.epam.convperf.test.tools.datamodel.Item;
import com.epam.convperf.test.tools.datamodel.Work;

/**
 * The implementation of the data reader and content store system for the MARC format
 * @author Sergey_Kandyrin
 * 
 * @see ContentDataProviderI 
 *
 */
public class MarcDataProvider implements ContentDataProviderI {

	private static Logger logger = Logger.getLogger(MarcDataProvider.class);
	
	static private MarcStreamReader reader;
	static private ArrayList<Agent> agentList = new ArrayList<>();
	static private int agentIndex = 0;
	static private ArrayList<Concept> conceptList = new ArrayList<>();
	static private int conceptIndex = 0;
	
	
	private static JSONObject agentDataModel;
	private static JSONObject agentMutationRule;
	private static JSONObject conceptDataModel;
	private static JSONObject conceptMutationRule;
	private static JSONObject workDataModel;
	private static JSONObject workMutationRule;
	
	//static resource initialization 
	//each content provider may use personaly configuration (reserved)
	static {
		JSONParser parser = new JSONParser();
		//URL resource;
		Object obj = null;

		try {

			obj = parser.parse(new FileReader("agent.json"));
			agentDataModel =  (JSONObject) obj;

			obj = parser.parse(new FileReader("agent_mutation_rule.json"));
			agentMutationRule =  (JSONObject) obj;

			obj = parser.parse(new FileReader("concept.json"));
			conceptDataModel =  (JSONObject) obj;

			obj = parser.parse(new FileReader("concept_mutation_rule.json"));
			conceptMutationRule =  (JSONObject) obj;

			obj = parser.parse(new FileReader("work.json"));
			workDataModel =  (JSONObject) obj;

			obj = parser.parse(new FileReader("work_mutation_rule.json"));
			workMutationRule =  (JSONObject) obj;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public ArrayList<Agent> getAgentList() {
		return agentList;
	}

	@Override
	public ArrayList<Concept> getConceptList() {
		return conceptList;
	}

	@Override
	public synchronized Agent getNextAgent() {
		if(agentIndex <= agentList.size()-1) {
			return agentList.get(agentIndex++);
		}
		return null;
	}

	@Override
	public synchronized Concept getNextConcept() {
		if(conceptIndex <= conceptList.size()-1) {
			return conceptList.get(conceptIndex++);
		}
		return null;
	}

	@Override
	public Work getNextWork() {
		while (reader.hasNext()) {
			Record rec = reader.next();
			if(rec.getVariableField(Work.WORK_MARC_FIELD) != null) {
				Work work = new Work();
				for (VariableField vf : rec.getVariableFields()) {
					String tag = vf.getTag();
					String value = vf.toString().replace(tag, "");
					work.addDataField(tag, value);
					System.out.println(String.format("TAG  : %s, VALUE %s", tag, value) );
				}
				return work;
			}
		}
		return null;
	}

	@Override
	public Instance getNextInstanceByCurrentWork() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Item getNextItemByCurrentWork() {
		// TODO Auto-generated method stub
		return null;
	}

	public MarcDataProvider(File marcResource) throws FileNotFoundException {
		synchronized (this) {
			if (reader == null) {
				// need to load agent and concept separately
				reader = new MarcStreamReader(new FileInputStream(marcResource));
				preLoadResource();
				reader = new MarcStreamReader(new FileInputStream(marcResource));
			}
		}

	}

	private void preLoadResource() {
		while (reader.hasNext()) {
			Record rec = reader.next();

			if (rec.getVariableField(Concept.CONCEPT_MARC_FIELD) != null) {

				String conceptRawText = rec.getVariableField(Concept.CONCEPT_MARC_FIELD).toString().trim();
				if (!conceptRawText.equals("")) {
					conceptList.addAll(Concept.parseConcepts(conceptRawText));
				}

			}
			if (rec.getVariableField(Agent.AGENT_MARC_FIELD) != null) {
				String agentRawText = rec.getVariableField(Agent.AGENT_MARC_FIELD).toString().trim();
				if (!agentRawText.equals("")) {
					agentList.addAll(Agent.parseAgents(agentRawText));
				}
			}
		}

	}
	
	public static JSONObject mergeConceptEntity(HashMap<String, String> data) {
		return ContentEngineDOMapper.mergeEntity(conceptDataModel, conceptMutationRule, data);
	}
	
	public static JSONObject mergeAgentEntity(HashMap<String, String> data) {
		return ContentEngineDOMapper.mergeEntity(agentDataModel, agentMutationRule, data);
	}
	
	public static JSONObject mergeWorkEntity(HashMap<String, String> data) {
		return ContentEngineDOMapper.mergeEntity(workDataModel, workMutationRule, data);
	}
	
	
	

	@Override
	public void updateAgentID(String oldUID, String registredUID) {
		for (Agent agent : agentList) {
			if(agent.getUID().equals(oldUID)) {
				agent.setUID(registredUID);
				logger.debug(String.format("Update agent uid %s -> %s", oldUID, registredUID));
				break;
			}
		}
		
	}

	@Override
	public void updateConceptID(String oldUID, String registredUID) {
		for (Concept concept : conceptList) {
			if(concept.getUID().equals(oldUID)) {
				concept.setUID(registredUID);
				logger.debug(String.format("Update concept uid %s -> %s", oldUID, registredUID));
				break;
			}
		}
		
	};
}
