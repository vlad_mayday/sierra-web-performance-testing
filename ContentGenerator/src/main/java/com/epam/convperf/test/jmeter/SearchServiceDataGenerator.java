package com.epam.convperf.test.jmeter;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.log4j.Logger;

import com.epam.convperf.test.tools.BibFrameDataProvider;
import com.epam.convperf.test.tools.ContentDataProviderI;
import com.epam.convperf.test.tools.MarcDataProvider;
import com.epam.convperf.test.tools.RandomDataProvider;
import com.epam.convperf.test.tools.datamodel.Agent;
import com.epam.convperf.test.tools.datamodel.Concept;
import com.epam.convperf.test.tools.datamodel.Work;

/**
 *  A unified interface for linking jmeter scripts and data provider API
 * @author Sergey_Kandyrin
 *
 */
public class SearchServiceDataGenerator extends AbstractJavaSamplerClient {

	private static ContentDataProviderI dataProvider;
	private static Logger logger = Logger.getLogger(SearchServiceDataGenerator.class);

	//Thread safe initialisation for data provider
	private static synchronized void initResource(JavaSamplerContext context) throws FileNotFoundException {

		if (dataProvider == null) {
			String provider = context.getParameter("CONTENT_PROVIDER");
			String resource = context.getParameter("CONTENT_RESOURCE");

			if ("MARC".equals(provider)) {
				dataProvider = new MarcDataProvider(new File(resource));

			}
			if ("BIBFRAME".equals(provider)) {
				dataProvider = new BibFrameDataProvider(new File(resource));

			}
			if ("RANDOM".equals(provider)) {
				dataProvider = new RandomDataProvider();

			}
		}
	}

	@Override
	public void setupTest(JavaSamplerContext context) {

		try {
			initResource(context);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			JMeterContextService.endTest();
		}
		super.setupTest(context);

	}

	/**
	 * @param CONTENT_PROVIDER  can take on values:  MARC|BIBFRAME|RANDOM
	 * @param CONTENT_RESOURCE  File path for MARC and BIBFRAME provider
	 * @param ACTION  can take on values: INIT|EXPORT|UPDATE
	 * INIT is used for content provider initialization (can take a few moment)
	 * EXPORT is used for generation a new entity
	 * UPDATE is user for update entity-ID after content creation on server side
	 * @param ENTITY_TYPE  can take on values: AGENT|CONCEPT|WORK|INSTANS|ITEM
	 */
	@Override
	public Arguments getDefaultParameters() {
		Arguments defaultParameters = new Arguments();
		defaultParameters.addArgument("CONTENT_PROVIDER", "${__P(CONTENT_PROVIDER, MARC)}");
		defaultParameters.addArgument("CONTENT_RESOURCE", "${__P(CONTENT_RESOURCE, FILE_PATH)}");
		defaultParameters.addArgument("ACTION", "${__P(ACTION, INIT)}");
		defaultParameters.addArgument("ENTITY_TYPE", "${__P(ENTITY_TYPE, CONCEPT)}");
		return defaultParameters;
	}

	@Override
	public SampleResult runTest(JavaSamplerContext arg0) {

		String action = arg0.getParameter("ACTION");
		String entityType = arg0.getParameter("ENTITY_TYPE");

		if ("INIT".equals(action)) {

		}
		if ("EXPORT".equals(action)) {
			if ("CONCEPT".equals(entityType)) {
				Concept concept = dataProvider.getNextConcept();
				JMeterContextService.getContext().getVariables().put("CONCEPT_UID", "" + concept.getUID());
				JMeterContextService.getContext().getVariables().put("CONCEPT", concept.toJson());
				logger.debug(concept.getUID());
				logger.debug(concept.toJson());
			}
			if ("AGENT".equals(entityType)) {
				Agent agent = dataProvider.getNextAgent();
				JMeterContextService.getContext().getVariables().put("AGENT_UID", "" + agent.getUID());
				JMeterContextService.getContext().getVariables().put("AGENT", agent.toJson());
				logger.debug(agent.getUID());
				logger.debug(agent.toJson());
			}
			if ("WORK".equals(entityType)) {
				Work agent = dataProvider.getNextWork();
				JMeterContextService.getContext().getVariables().put("WORK_UID", "" + agent.getUID());
				JMeterContextService.getContext().getVariables().put("AGENT", agent.toJson());
				logger.debug(agent.getUID());
				logger.debug(agent.toJson());
			}
		}
		if ("UPDATE".equals(action)) {
			if ("CONCEPT".equals(entityType)) {
				String oldUID = JMeterContextService.getContext().getVariables().get("CONCEPT_UID");
				String registredUID = JMeterContextService.getContext().getVariables().get("REGISTRED_CONCEPT_UID");
				dataProvider.updateConceptID(oldUID, registredUID);
			}

			if ("AGENT".equals(entityType)) {
				String oldUID = JMeterContextService.getContext().getVariables().get("AGENT_UID");
				String registredUID = JMeterContextService.getContext().getVariables().get("REGISTRED_AGENT_UID");
				dataProvider.updateAgentID(oldUID, registredUID);
			}
		}

		return null;

	}

	@Override
	public void teardownTest(JavaSamplerContext context) {
	}

}