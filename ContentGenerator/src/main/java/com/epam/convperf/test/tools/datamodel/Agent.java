package com.epam.convperf.test.tools.datamodel;

import java.util.ArrayList;

import com.epam.convperf.test.tools.MarcDataProvider;

public class Agent extends CEEntity{
	public static String AGENT_MARC_FIELD = "100";
		
	public Agent() {
		super();
	}
	public static ArrayList<Agent> parseAgents(String rawAgentText){
		rawAgentText = rawAgentText.replaceAll(".*\\$a", "").replaceAll("\\$d.*", "").replace("$q", " ");
		
		ArrayList<Agent> agentList = new ArrayList<>();
		
		Agent uniqAgent = new Agent();
		uniqAgent.addDataField(AGENT_MARC_FIELD, rawAgentText);
		agentList.add(uniqAgent);
		
		return agentList;
		
	}
	
	public void addDataField(String type, String value) {
		data.put(type, value);
	}
	
	public String toJson() {
		return MarcDataProvider.mergeAgentEntity(data).toJSONString();
	}
	
	
}
