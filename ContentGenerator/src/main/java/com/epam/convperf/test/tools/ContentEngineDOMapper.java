package com.epam.convperf.test.tools;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * The class that performs  filling the resulting entity by corresponding ruls
 * @author Sergey_Kandyrin
 *
 */
public class ContentEngineDOMapper {

	@SuppressWarnings("unchecked")
	public static JSONObject mergeEntity(JSONObject jModel, JSONObject jRule, HashMap<String, String> data) {

		JSONObject ret = (JSONObject) jModel.clone();

		Set<String> keys = data.keySet();
		Iterator<String> i = keys.iterator();
		while (i.hasNext()) {
			String key = i.next();
			String value = data.get(key);
			if (jRule.containsKey(key)) {
				String targetJsonElement = jRule.get(key).toString();
				if (targetJsonElement.split(":").length == 1) {
					ret.put(targetJsonElement, value);
				} else {
					JSONObject subElement = ret;
					for (int inx = 0; inx < targetJsonElement.split(":").length; inx++) {
						String subE = targetJsonElement.split(":")[inx];
						if (inx == targetJsonElement.split(":").length - 1) {
							subElement.put(subE, value);
							break;
						} else {
							if (subElement.get(subE) instanceof JSONArray) {
								subElement = (JSONObject) ((JSONArray) ret.get(subE)).get(0);
							}
							if (subElement.get(subE) instanceof JSONObject) {
								subElement = (JSONObject) subElement.get(subE);
							}
						}

					}
				}

			}
		}
		return ret;
	}

}
