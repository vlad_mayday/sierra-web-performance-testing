#!/bin/bash
randNum=$(shuf -i 9000-60000 -n 1)
currentPort=$(awk -F= '/^jmeterPlugin.sts.port=[[:digit:]]*$/{print $2}' jmeter.properties)
while [ $randNum -eq $currentPort ]
do
echo "EQUALS"
randNum=$(shuf -i 9000-60000 -n 1)
done
find -type f -name jmeter.properties -exec sed -i -r "s/^jmeterPlugin.sts.port=[[:print:]]*/jmeterPlugin.sts.port=${randNum}/g" {} \;