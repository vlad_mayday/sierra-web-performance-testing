sum=0
for j in $(pgrep -f "sierra -Z" | xargs -I replace ps -p replace -o %mem --no-headers)
do
sum=$(awk "BEGIN {print $sum+$j; exit}")
done
echo $sum
