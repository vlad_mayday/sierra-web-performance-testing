#!/bin/bash

check_in=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*BT_CHECK_IN,\([0-9]*\),.*/\1/p')
check_in_no_patron=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*BT_CHECK_IN_NO_PATRON,\([0-9]*\),.*/\1/p')
check_ins=$((check_in+check_in_no_patron))

check_out_single=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*BT_CHECK_OUT_SINGLE_ITEM,\([0-9]*\),.*/\1/p')
check_out_multiple=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*BT_CHECK_OUT_MULTIPLE_ITEMS,\([0-9]*\),.*/\1/p')
check_outs=$((check_out_single+check_out_multiple))

search[1]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C111541-BT_SEARCH_ITEM_BY_AUTHOR,\([0-9]*\),.*/\1/p')
search[2]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C111638-BT_SEARCH_PATRON_BY_BARCODE,\([0-9]*\),.*/\1/p')
search[3]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C000004-BT_SEARCH_ITEM_BY_KEYWORD,\([0-9]*\),.*/\1/p')
search[4]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C112407-BT_SEARCH_PATRON_BY_BARCODE,\([0-9]*\),.*/\1/p')
search[5]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C111604-BT_SEARCH_ITEM_BY_AUTHOR,\([0-9]*\),.*/\1/p')
search[6]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C111600-BT_SEARCH_ITEM_BY_AUTHOR,\([0-9]*\),.*/\1/p')
search[7]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C000003-BT_SEARCH_ITEM_BY_CALL_NUMBER,\([0-9]*\),.*/\1/p')
search[8]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C000002-BT_SEARCH_ITEM_BY_SUBJECT,\([0-9]*\),.*/\1/p')
search[9]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C111600-BT_SEARCH_ITEM_BY_BARCODE,\([0-9]*\),.*/\1/p')
search[10]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C000001-BT_SEARCH_ITEM_BY_TITLE,\([0-9]*\),.*/\1/p')
search[11]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C112406-BT_SEARCH_PATRON_BY_BARCODE,\([0-9]*\),.*/\1/p')
search[12]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C111601-BT_SEARCH_ITEM_BY_AUTHOR,\([0-9]*\),.*/\1/p')
search[13]=$(cat $BUILD_NUMBER/agg_report.csv | sed -n 's/.*C112831-BT_SEARCH_PATRON_BY_BARCODE,\([0-9]*\),.*/\1/p')
search_aggr=0
search_simple=$((search[1]+search[3]+search[5]+search[6]+search[7]+search[8]+search[10]+search[12]))

for varible in ${search[*]}
        do
                search_aggr=$((search_aggr+varible))
        done

echo "Performance KPI - Business transactions"
echo "Check_in = $check_ins"
echo "Check_out = $check_outs"
echo "Aggregated_search = $search_aggr"
echo "Search_(without search by barcode) = $search_simple"
