#!/bin/bash

#Kill monitoring processes

for ps in $(ps -ax | grep docker_cont_monitor.sh | grep -v 'auto' | sed 's/\(^[0-9]+\).*/\1/')
do
	kill -9 $ps
done

#Parse files with monitoring data

#arr[0]=1
#arr[1]=2
#arr[3]=3
#arr[4]=4

numOfDockers=$(docker ps | grep -c tomcat)

#for num in ${arr[*]}
for ((i=0; i<$numOfDockers; i++))
do

num=$(expr $i + 1)

address1="/home/centos/DOCKER$num"
address2="_CPU.jtl"
address3="_CPU_temp.jtl"

awk -F, '{$2=$2*1000;print}' OFS=, $address1$address2 > $address1$address3

address4="_MEM.jtl"
address5="_MEM_temp.jtl"

awk -F, '{$2=$2*1000;print}' OFS=, $address1$address4 > $address1$address5

mv $address1$address3 $address1$address2
mv $address1$address5 $address1$address4


sed -i '1s/^/timeStamp,elapsed,label,responseCode,responseMessage,threadName,dataType,success,failureMessage,bytes,grpThreads,allThreads,Latency,IdleTime\n/' $address1$address2

sed -i '1s/^/timeStamp,elapsed,label,responseCode,responseMessage,threadName,dataType,success,failureMessage,bytes,grpThreads,allThreads,Latency,IdleTime\n/' $address1$address4

done



