#!/bin/bash

DOCKERS=($(docker ps | grep tomcat | sed -E "s/([a-z0-9]+) .+/\1/"))

for ((i=0; i<${#DOCKERS[@]}; i++))
do

        docker_num=$(expr $i + 1)

        docker stats --all --format "{{.CPUPerc}}" ${DOCKERS[i]} | while IFS= read -r line; do printf '%s\n' "$(date +%s%3N),$(echo $line | perl -n -e'/^.*?(\d+\.\d+)%/ && print $1'),\"DOCKER_${DOCKERS[i]}_CPU\",,,,,true,,0,0,0,0,0"; done > DOCKER${docker_num}_CPU.jtl &

        docker stats --all --format "{{.MemPerc}}" ${DOCKERS[i]} | while IFS= read -r line; do printf '%s\n' "$(date +%s%3N),$(echo $line | perl -n -e'/^.*?(\d+\.\d+)%/ && print $1'),\"DOCKER_${DOCKERS[i]}_MEM\",,,,,true,,0,0,0,0,0"; done > DOCKER${docker_num}_MEM.jtl &

done
