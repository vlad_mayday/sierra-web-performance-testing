import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.marc4j.MarcReader;
import org.marc4j.MarcStreamReader;
import org.marc4j.MarcStreamWriter;
import org.marc4j.MarcWriter;
import org.marc4j.marc.*;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Application {

    static String conceptTags = "148|150|151|155";
    static String agentTags = "100|110";
    static String itemTag = "852";

    static int multiplyRate = 1;

    public static void main(String[] args) throws FileNotFoundException {
        InputStream in = new FileInputStream("d:\\III-PERF\\sample\\sample_marc.mrc\\10000-1.mrc");
        MarcReader reader = new MarcStreamReader(in);

        OutputStream out = new FileOutputStream("d:\\III-PERF\\sample\\sample_marc.mrc\\10000-1_items.mrc");
        MarcWriter writer = new MarcStreamWriter(out);

        HashMap<String, Integer> counter = new HashMap<>();
        while (reader.hasNext()) {
            Record record = reader.next();
            final MarcFactory factory = MarcFactory.newInstance();

            List<DataField> dataFields = record.getDataFields();
            List<ControlField> controlFields = record.getControlFields();

            // Change record ID with random value
            for (int i = 0; i < multiplyRate; i++) {
                String field001New = "";
                String field001 = "";
                for (ControlField field : controlFields) {
                    if (field.getTag().equals("001")) {
                        field001 = field.getData();
                        String prefix = field001.substring(0, 6);
                        if (counter.get(prefix) == null) {
                            counter.put(prefix, 0);
                        } else {
                            int count = counter.get(prefix);
                            counter.put(prefix, ++count);
                        }
                        String numFormatted = StringUtils.leftPad(counter.get(prefix).toString(), (field001.length() - 6), '0');
                        field001New = prefix + numFormatted;
                        field.setData(field001New);

                    } else if (field.getTag().equals("010")) {
                        field.setData(field.getData().replace(field001, field001New));
                    }
                }

                // Add subfields to data fields
                dataFields.stream()
                        .peek(d -> {
                            if (d.getTag().matches(conceptTags)) {
                                List<Subfield> subFields = d.getSubfields();
                                if (subFields.size() > 1) {
                                    subFields.get(1).setData(RandomStringUtils.random(10, true, true));
                                    //subFields.forEach(s -> s.setData(s.getData() + RandomStringUtils.random(10, true, true)));
                                } else {
                                    d.addSubfield(factory.newSubfield('a', RandomStringUtils.random(10, true, true)));
                                }
                            } else if (d.getTag().matches(itemTag)) {
                                List<Subfield> subFields = d.getSubfields();
                                List<Character> codes = subFields.stream().map(Subfield::getCode).collect(Collectors.toList());
                                if (!codes.contains('c'))
                                    d.addSubfield(factory.newSubfield('c', RandomStringUtils.random(4, true, false)));
                                if (!codes.contains('d'))
                                    d.addSubfield(factory.newSubfield('d', Integer.valueOf(RandomUtils.nextInt(10000000, 99999999)).toString()));
                                if (!codes.contains('e'))
                                    d.addSubfield(factory.newSubfield('e', RandomStringUtils.random(1, true, false)));
                                if (!codes.contains('g'))
                                    d.addSubfield(factory.newSubfield('g', Integer.valueOf(RandomUtils.nextInt(10000000, 99999999)).toString()));
                                if (!codes.contains('i'))
                                    d.addSubfield(factory.newSubfield('i', Integer.valueOf(RandomUtils.nextInt(0, 9)).toString()));
                            }
                        }).collect(Collectors.toList());

                final Record record2 = factory.newRecord();
                controlFields.forEach(record2::addVariableField);
                dataFields.forEach(record2::addVariableField);
                writer.write(record2);
            }
        }
        writer.close();
    }
}
