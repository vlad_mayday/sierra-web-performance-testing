import time
import sys
from datetime import datetime

f = open(sys.argv[1], 'r')
result = open(sys.argv[1]+'.parsed.csv', 'w')

postgressSum = 0
appacheSum = 0

result.write('postgres,appache\n')

for line in f:
    if '------------' in line:
        result.write(str(postgressSum) +','+ str(appacheSum))
        postgressSum = 0
        appacheSum = 0
        result.write('\n')
    if 'postgres:' in line:
        postgressSum += float(line[16:21])
    if 'apache-2.4.18' in line:
        appacheSum += float(line[16:21])
	