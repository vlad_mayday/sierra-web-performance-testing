import sys
f = open(sys.argv[1], 'r')
result = open(sys.argv[1]+'.parsed.csv', 'w')

for line in f:
    newString = line[line.find('"pointlist":[') + len('"pointlist":['):line.find(',"expression"')].replace('[','').replace(']','').split(',')

counter = 0

header = 'timeStamp,elapsed,label,responseCode,responseMessage,threadName,success,bytes,grpThreads,allThreads,Latency,Hostname\n'
print(header)
result.write(header)
for x in newString:
    if counter % 2 == 0:
        output = x[0:13] + ','
    else:
        output += str(int(float(x)*1000)) + ',"APPDOG",,,,true,0,0,0,0,host\n'
        result.write(output)
    counter += 1
        