import sys

f = open(sys.argv[1], 'r')
counter = 0
errorTime = 0
errorCounter = 0
samplerTime = 0
startErrorTime = 0
errorSmallTime = 0

for line in f:
    if counter != 0 and line.split(',')[7] == 'false':
        samplerTime = int(line.split(',')[0]) + int(line.split(',')[1])
        if errorCounter == (counter - 1):
            errorSmallTime = samplerTime - startErrorTime
        else:
            startErrorTime = samplerTime
            # print ('startErrorTime = ' + str(startErrorTime))
        errorCounter = counter
    else:
        # if errorSmallTime != 0:
            # print ('errorSmallTime = ' + str(errorSmallTime))
        errorTime += errorSmallTime
        errorSmallTime = 0
    counter += 1
errorTime += errorSmallTime
print (str(errorTime)[:-3])