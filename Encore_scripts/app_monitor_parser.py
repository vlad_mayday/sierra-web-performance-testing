import time
import sys
from datetime import datetime

f = open(sys.argv[1], 'r')

trigger = 0

for line in f:
    if '------------' in line:
        trigger = 1
        continue
    if trigger == 1:
        currTime = line[0:20]
        result = open(currTime.replace(':','-').replace(' ','_') + sys.argv[1] +'.txt', 'w')
        trigger = 2
    if trigger == 2:
        result.write(line)